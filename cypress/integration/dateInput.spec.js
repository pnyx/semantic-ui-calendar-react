import * as Constant from "../support/common/constants";
const moment = require("moment");
/// <reference types="cypress" />
context("Test Date Input field DD-MM-YYY", () => {
  beforeEach(() => {
    cy.visit(Constant.BASE_URL);
  });
  it("Date input with null value should show null message", () => {
    cy.get('input[name="dateInput"]').should("have.value", "");
  });
  it("Clear and Enter date with keyboard", () => {
    cy.clearInputByName("dateInput");
    cy.get('input[name="dateInput"]').type("21-12-1921");
    cy.get('input[name="dateInput"]').should("have.value", "21-12-1921");
  });
  it("Successfully Correct year with keyboard", () => {
    cy.clearInputByName("dateInput");
    cy.get('input[name="dateInput"]').type("27-07-1945");
    cy.get('input[name="dateInput"]')
      .type("{backspace}")
      .type("1");
    cy.get('input[name="dateInput"]').should("have.value", "27-07-1941");
  });
  it("Successfully Correct month with keyboard", () => {
    cy.clearInputByName("dateInput");
    cy.get('input[name="dateInput"]').type("27-07-1945");
    cy.get('input[name="dateInput"]')
      .type("{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}")
      .type("{backspace}")
      .type("1");
    cy.get('input[name="dateInput"]').should("have.value", "27-01-1945");
  });
  it("Fail to Correct bad date with keyboard", () => {
    cy.clearInputByName("dateInput");
    cy.get('input[name="dateInput"]').type("30-02-1921");
    cy.get('div[class="error"]')
      .contains("error")
      .should("exist");
  });
  it("Clear and Enter date with mouse", () => {
    cy.clearInputByName("dateInput");
    const currentYear = moment().year();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get('[data-test="btn_header_title"]').click();
    cy.get(`[data-test="year_${currentYear - 1}"]`).click();
    cy.get(`[data-test="month_1"]`).click();
    cy.get(`[data-test="date_1"]`).click();

    cy.get('input[name="dateInput"]').should(
      "have.value",
      `01-01-${currentYear - 1}`
    );
  });
  it("Update month with mouse", () => {
    const currentYear = moment().year();
    cy.get('input[name="dateInput"]').click();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="date_4"]`).click();
    cy.get('input[name="dateInput"]').should(
      "have.value",
      `04-03-${currentYear}`
    );
  });
  it("Update month with btn_header_next", () => {
    const currentYear = moment().year();
    cy.get('input[name="dateInput"]').click();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="btn_header_next"]`).click();
    cy.get(`[data-test="date_4"]`).click({force: true});
    cy.get('input[name="dateInput"]').should(
      "have.value",
      `04-04-${currentYear}`
    );
  });
  it("Update month with btn_header_back", () => {
    const currentYear = moment().year();
    cy.get('input[name="dateInput"]').click();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="btn_header_back"]`).click();
    cy.get(`[data-test="date_4"]`).click();
    cy.get('input[name="dateInput"]').should(
      "have.value",
      `04-02-${currentYear}`
    );
  });
  it("Update year with mouse", () => {
    const currentYear = moment().year();
    cy.get('input[name="dateInput"]').click();
    cy.get('[data-test="btn_footer_year"]').click();
    cy.get(`[data-test="year_${currentYear - 3}"]`).click();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="date_4"]`).click();

    cy.get('input[name="dateInput"]').should(
      "have.value",
      `04-03-${currentYear - 3}`
    );
  });
  it("Update year with btn_header_next", () => {
    const currentYear = moment().year();
    cy.get('input[name="dateTimeInput"]').click();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="btn_header_next"]`).click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="date_4"]`).click();
    cy.get(`[data-test="hour_08"]`).click();
    cy.get(`[data-test="minute_30"]`).click();
    cy.get('input[name="dateTimeInput"]').should(
      "have.value",
      `04-03-${currentYear +1} 08:30`
    );
  });
  it("Update year with btn_header_back", () => {
    const currentYear = moment().year();
    cy.get('input[name="dateTimeInput"]').click();
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="btn_header_back"]`).click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="date_4"]`).click();
    cy.get(`[data-test="hour_08"]`).click();
    cy.get(`[data-test="minute_30"]`).click();

    cy.get('input[name="dateTimeInput"]').should(
      "have.value",
      `04-03-${currentYear - 1} 08:30`
    );
  });
  it("Update date mixing keyboard and mouse", () => {
    cy.clearInputByName("dateInput");
    cy.get('input[name="dateInput"]').type("21-12-1921");
    cy.get('input[name="dateInput"]').should("have.value", "21-12-1921");
    cy.get('[data-test="btn_footer_month"]').click();
    cy.get(`[data-test="month_3"]`).click();
    cy.get(`[data-test="date_4"]`).click();
    cy.get('input[name="dateInput"]').should("have.value", `04-03-1921`);
  });
  
});
