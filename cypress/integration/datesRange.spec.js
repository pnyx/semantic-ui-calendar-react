import * as Constant from "../support/common/constants";
const moment = require("moment");
const { padStart } = require("lodash");

/// <reference types="cypress" />
context("Test Date Input field DD-MM-YYY", () => {
  beforeEach(() => {
    cy.visit(Constant.BASE_URL);
  });
  it("Clear and Enter date with keyboard", () => {
    const currentYear = moment().year();
    cy.clearInputByName("datesRange");
    cy.get('input[name="datesRange"]').type(
      `01-06-${currentYear - 1} - 01-06-${currentYear}`
    );
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `01-06-${currentYear - 1} - 01-06-${currentYear}`
    );
    cy.get('div[class="error"]')
      .contains("validated")
      .should("exist");
  });
  it("Successfully Correct year with keyboard", () => {
    cy.clearInputByName("datesRange");
    cy.get('input[name="datesRange"]').type("27-07-1945 - 27-08-1945");
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]')
      .type("{backspace}")
      .type("6");
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      "27-07-1945 - 27-08-1946"
    );
    cy.get('div[class="error"]')
      .contains("validated")
      .should("exist");
  });
  it("Successfully Correct month with keyboard", () => {
    cy.clearInputByName("datesRange");
    cy.get('input[name="datesRange"]').type("27-07-1945 - 27-08-1945");
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]')
      .type("{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}")
      .type("{backspace}")
      .type("9");
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      "27-07-1945 - 27-09-1945"
    );
    cy.get('div[class="error"]')
      .contains("validated")
      .should("exist");
  });
  it("Fail to Correct bad date with keyboard", () => {
    cy.clearInputByName("datesRange");
    cy.get('input[name="datesRange"]').type("02-02-1945 - 30-02-1945");
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('div[class="error"]')
      .contains("error")
      .should("exist");
  });
  it("Clear and Enter date with mouse", () => {
    const lastYear = moment(Date.now())
      .subtract(1, "month")
      .year();
    const lastMonth = padStart(
      moment(Date.now())
        .subtract(1, "month")
        .month() + 1,
      2,
      0
    );
    const nextYear = moment(Date.now())
      .add(1, "month")
      .year();
    const nextMonth = padStart(
      moment(Date.now())
        .add(1, "month")
        .month() + 1,
      2,
      0
    );
    cy.get('input[name="datesRange"]').click();
    cy.get('[data-test="btn_header_back"]').click();
    cy.get(`[data-test="date_10"]`).click();
    cy.get(`[data-test="date_10"]`).click();
    cy.get('[data-test="btn_header_next"]').click();
    cy.get('[data-test="btn_header_next"]').click();
    cy.get(`[data-test="date_15"]`).click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `10-${lastMonth}-${lastYear} - 15-${nextMonth}-${nextYear}`
    );
  });
  it("Enter Today date with mouse", () => {
    const currentDay = padStart(moment(Date.now()).date(), 2, 0);
    const currentMonth = padStart(moment(Date.now()).month() + 1, 2, 0);
    const currentYear = moment(Date.now()).year();
    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("Today")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `${currentDay}-${currentMonth}-${currentYear} - ${currentDay}-${currentMonth}-${currentYear}`
    );
  });
  it("Enter Yesterday date with mouse", () => {
    const yesterdayDay = padStart(
      moment(Date.now())
        .subtract(1, "day")
        .date(),
      2,
      0
    );
    const yesterdayMonth = padStart(
      moment(Date.now())
        .subtract(1, "day")
        .month() + 1,
      2,
      0
    );
    const yesterdayYear = moment(Date.now())
      .subtract(1, "day")
      .year();
    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("Yesterday")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `${yesterdayDay}-${yesterdayMonth}-${yesterdayYear} - ${yesterdayDay}-${yesterdayMonth}-${yesterdayYear}`
    );
  });
  it("Enter This Week date with mouse", () => {
    const startDay = padStart(
      moment(Date.now())
        .set({ day: 1 })
        .date(),
      2,
      0
    );
    const startMonth = padStart(
      moment(Date.now())
        .set({ day: 1 })
        .month() + 1,
      2,
      0
    );
    const startYear = moment(Date.now())
      .set({ day: 1 })
      .year();
    const endDay = padStart(
      moment(Date.now())
        .set({ day: 7 })
        .date(),
      2,
      0
    );
    const endMonth = padStart(
      moment(Date.now())
        .set({ day: 7 })
        .month() + 1,
      2,
      0
    );
    const endYear = moment(Date.now())
      .set({ day: 7 })
      .year();
    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("This Week")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `${startDay}-${startMonth}-${startYear} - ${endDay}-${endMonth}-${endYear}`
    );
  });
  it("Enter Last Week date with mouse", () => {
    const startDay = padStart(
      moment(Date.now())
        .set({ day: -6 })
        .date(),
      2,
      0
    );
    const startMonth = padStart(
      moment(Date.now())
        .set({ day: -6 })
        .month() + 1,
      2,
      0
    );
    const startYear = moment(Date.now())
      .set({ day: -6 })
      .year();
    const endDay = padStart(
      moment(Date.now())
        .set({ day: 0 })
        .date(),
      2,
      0
    );
    const endMonth = padStart(
      moment(Date.now())
        .set({ day: 0 })
        .month() + 1,
      2,
      0
    );
    const endYear = moment(Date.now())
      .set({ day: 0 })
      .year();
    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("Last Week")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `${startDay}-${startMonth}-${startYear} - ${endDay}-${endMonth}-${endYear}`
    );
  });
  it("Enter Last 2 Week date with mouse", () => {
    const startDay = padStart(
      moment(Date.now())
        .set({ day: -13 })
        .date(),
      2,
      0
    );
    const startMonth = padStart(
      moment(Date.now())
        .set({ day: -13 })
        .month() + 1,
      2,
      0
    );
    const startYear = moment(Date.now())
      .set({ day: -13 })
      .year();
    const endDay = padStart(
      moment(Date.now())
        .set({ day: 0 })
        .date(),
      2,
      0
    );
    const endMonth = padStart(
      moment(Date.now())
        .set({ day: 0 })
        .month() + 1,
      2,
      0
    );
    const endYear = moment(Date.now())
      .set({ day: 0 })
      .year();
    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("Last 2 Weeks")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `${startDay}-${startMonth}-${startYear} - ${endDay}-${endMonth}-${endYear}`
    );
  });
  it("Enter This Month date with mouse", () => {
    const thisMonth = padStart(moment(Date.now()).month() + 1, 2, 0);
    const thisYear = moment(Date.now()).year();
    const endDay = padStart(
      moment(Date.now())
        .endOf("month")
        .date(),
      2,
      0
    );
    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("This month")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `01-${thisMonth}-${thisYear} - ${endDay}-${thisMonth}-${thisYear}`
    );
  });
  it("Enter Last Month date with mouse", () => {
    const startDayLastMonth = moment()
      .subtract(1, "months")
      .startOf("month")
      .startOf("day");

    const endDayLastMonth = moment()
      .subtract(1, "months")
      .endOf("month")
      .endOf("day");

    cy.get('input[name="datesRange"]').click();
    cy.get("button")
      .contains("Last month")
      .click();
    cy.get("button")
      .contains("Ok")
      .click();
    cy.get('input[name="datesRange"]').should(
      "have.value",
      `${startDayLastMonth.format("DD-MM-YYYY")} - ${endDayLastMonth.format(
        "DD-MM-YYYY"
      )}`
    );
  });
});
