import * as Constant from '../support/common/constants';
const moment = require('moment');
/// <reference types="cypress" />
context('Test Time Input field HH:MM', () => {
  beforeEach(() => {
    cy.visit(Constant.BASE_URL);
  });
  it('Clear and Enter time with keyboard', () => {
    cy.clearInputByName('timeInput');
    cy.get('input[name="timeInput"]').type('07:10 AM');
    cy.get('input[name="timeInput"]').should('have.value', '07:10 AM');
  });
  it('Successfully Correct minute with keyboard', () => {
    cy.clearInputByName('timeInput');
    cy.get('input[name="timeInput"]').type('07:10 AM');
    cy.get('input[name="timeInput"]')
      .type('{leftarrow}{leftarrow}{leftarrow}')
      .type('{backspace}')
      .type('1');
    cy.get('input[name="timeInput"]').should('have.value', '07:11 AM');
  });
  it('Successfully Correct hour with keyboard', () => {
    cy.clearInputByName('timeInput');
    cy.get('input[name="timeInput"]').type('07:10 PM');
    cy.get('input[name="timeInput"]')
      .type(
        '{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}',
      )
      .type('{backspace}')
      .type('8');
    cy.get('input[name="timeInput"]').should('have.value', '08:10 PM');
  });
  it('Fail to Correct bad time with keyboard', () => {
    cy.clearInputByName('timeInput');
    cy.get('input[name="timeInput"]').type('30:68 AM');
    cy.get('div[class="error"]')
      .contains('error')
      .should('exist');
  });
  it('Clear and Enter time with mouse', () => {
    cy.clearInputByName('timeInput');
    cy.get('[data-cy="pickerHeader"]').should('not.exist');
    cy.get('[data-test="btn_footer_hour"]').click();
    cy.get('[data-test="hour_13"]').click();
    cy.get(`[data-test="minute_35"]`).click();

    cy.get('input[name="timeInput"]').should('have.value', '01:35 PM');
  });
  it('Update minutes with mouse', () => {
    cy.get('input[name="timeInput"]').click();
    cy.get('[data-test="btn_footer_minute"]').click();
    cy.get(`[data-test="minute_20"]`).click();
    const currentHour = moment(Date.now()).format('hh');
    const meridiem = moment(Date.now()).format('A');
    cy.get('input[name="timeInput"]').should(
      'have.value',
      `${currentHour}:20 ${meridiem}`,
    );
  });
  it('Update time mixing keyboard and mouse', () => {
    cy.clearInputByName('timeInput');
    cy.get('input[name="timeInput"]').type('06:30 PM');
    cy.get('input[name="timeInput"]').should('have.value', '06:30 PM');
    cy.get('[data-test="btn_footer_minute"]').click();
    cy.get(`[data-test="minute_20"]`).click();
    cy.get('input[name="timeInput"]').should('have.value', '06:20 PM');
  });
});
