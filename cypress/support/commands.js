// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
const { forEach } = require("lodash");
Cypress.Commands.add("clickButton", (text, option = false) => {
  cy.get("button", { timeout: 10000 })
    .contains(text)
    .as("button");
  cy.get("@button").click();
  if ("wait" === option)
    cy.get("@button", { timeout: 10000 })
      .should("exist")
      .and("not.have.class", "loading");
  if ("remain" === option) return;
  else cy.get("@button", { timeout: 10000 }).should("not.exist");
});

Cypress.Commands.add("clearInputByName", selector => {
  cy.get(`input[name="${selector}"]`)
    .click()
    .type(' ')
    .clear()
});
