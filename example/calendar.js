import React, { useCallback, useState } from 'react';
import { Button, Popup, Icon } from 'semantic-ui-react';
import ReactDOM from 'react-dom';
import moment from 'moment';

import {
  CustomDatesRangeInput,
  DateInput,
  DateTimeInput,
  TimeInput,
  // DateInputPopup,
} from '../src';

moment.locale('en');
moment.updateLocale('en', {
  week: {
    dow: 1,
  },
});
// console.log('HERE');
const App = () => {
  const [datesRange, setDatesRange] = useState({
    start: moment(),
    end: moment(),
  });

  const [date, setDate] = useState(null);
  const [time, setTime] = useState(moment());
  const [dateTime, setDateTime] = useState(moment());
  const [message, setMessage] = useState('');
  const onDatesRangeChange = useCallback(
    datesRange => {
      setDatesRange(datesRange);
    },
    [datesRange, setDatesRange],
  );
  const onTimeInputChange = useCallback(
    time => {
      setTime(time);
    },
    [time, setTime],
  );
  const onDateChange = useCallback(
    date => {
      setDate(date);
    },
    [date, setDate],
  );
  const onDateTimeChange = useCallback(
    dateTime => {
      setDateTime(dateTime);
    },
    [dateTime, setDateTime],
  );
  const onValidated = useCallback(() => {
    setMessage('validated');
  }, []);
  const onValidatedError = useCallback(() => {
    setMessage('error');
  }, []);
  return (
    <div
    // style={{ height: '200px', overflow: 'scroll' }}
    >
      <div
      // style={{ height: '1500px' }}
      >
        <CustomDatesRangeInput
          dateFormat="DD-MM-YYYY"
          onValidateError={onValidatedError}
          onValidated={onValidated}
          placeholder="From - To"
          className="example-calendar-input"
          name="datesRange"
          iconPosition="left"
          datesRange={datesRange}
          onRangeChange={onDatesRangeChange}
        />
        <DateInput
          dateFormat="DD-MM-YYYY"
          placeholder="From"
          className="example-calendar-input"
          name="dateInput"
          iconPosition="left"
          value={date}
          onValidateError={onValidatedError}
          onValidated={onValidated}
          onChange={onDateChange}
          nullMessage="No Expired Date"
        />
        <DateInput
          dateFormat="DD-MM-YYYY"
          placeholder="From"
          className="example-calendar-input"
          name="dateInput"
          iconPosition="left"
          value={null}
          onValidateError={onValidatedError}
          onValidated={onValidated}
          onChange={onDateChange}
          nullMessage="No Expired Date"
          customTrigger={<div>Customize date input</div>}
        />
        <div className="error">{message}</div>
        <Popup
          trigger={<Button icon="add" />}
          content="Hello. This is an inverted popup"
          inverted
        />
        <Popup
          trigger={<Icon circular name="heart" />}
          content="Hello. This is an inverted popup"
          inverted
        />
        <DateTimeInput
          dateTimeFormat="DD-MM-YYYY hh:mm"
          placeholder="From"
          className="example-calendar-input"
          name="dateTimeInput"
          iconPosition="left"
          dateTimeValue={dateTime}
          onValidateError={onValidatedError}
          onValidated={onValidated}
          onDateTimeChange={onDateTimeChange}
          nullMessage="No Expired Date"
        />
        <TimeInput
          name="timeInput"
          value={time}
          onChange={onTimeInputChange}
          onValidateError={onValidatedError}
          onValidated={onValidated}
        />
      </div>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
