import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import ClassNames from 'classnames';
import PropTypes from 'prop-types';
import moment from 'moment';
import { toLower } from 'lodash';

class DatePickerCell extends Component {
  onCellClick = event => {
    event.stopPropagation();
    const { onClick, data } = this.props;
    onClick(event, { ...this.props, value: data });
  };
  render() {
    const { className, data, hasData, active, disabled } = this.props;
    const classes = ClassNames(className, 'suir-calendar', 'date');
    const strDate = data.format('D');
    return (
      <Table.Cell
        active={active}
        disabled={disabled}
        onClick={this.onCellClick}
        className={classes}
        {...(hasData ? { ['data-test']: `date_${toLower(strDate)}` } : {})}>
        {strDate}
      </Table.Cell>
    );
  }
}

DatePickerCell.propTypes = {
  data: PropTypes.instanceOf(moment).isRequired,
  className: PropTypes.string,
  /** (event, data) => {} */
  onClick: PropTypes.func,
  hasData: PropTypes.bool,
};

export default DatePickerCell;
