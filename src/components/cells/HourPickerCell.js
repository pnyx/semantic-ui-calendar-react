import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';
import { toLower } from 'lodash';

class HourPickerCell extends Component {
  _onHourClick = event => {
    event.stopPropagation();
    const { onClick, hour } = this.props;
    onClick(event, { ...this.props, value: hour });
  };
  render() {
    const { hour, active, disabled } = this.props;
    return (
      <Table.Cell
        active={active}
        disabled={disabled}
        onClick={this._onHourClick}
        className="suir-calendar time"
        textAlign="center"
        data-test={`hour_${toLower(hour)}`}
      >
        {hour + ':00'}
      </Table.Cell>
    );
  }
}
HourPickerCell.propTypes = {
  /** (event, data) => {} */
  onClick: PropTypes.func.isRequired,
  hour: PropTypes.string.isRequired,
};

export default HourPickerCell;
