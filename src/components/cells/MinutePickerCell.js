import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';
import { toLower } from 'lodash';

class MinutePickerCell extends Component {
  _onMinuteClick = event => {
    event.stopPropagation();
    const { onClick, minute } = this.props;
    onClick(event, { ...this.props, value: minute });
  };
  render() {
    const { hour, minute, active, disabled } = this.props;
    return (
      <Table.Cell
        active={active}
        disabled={disabled}
        onClick={this._onMinuteClick}
        className="suir-calendar time"
        textAlign="center"
        data-test={`minute_${toLower(minute)}`}
      >
        {hour + ':' + minute}
      </Table.Cell>
    );
  }
}

MinutePickerCell.propTypes = {
  /** (event, data) => {} */
  onClick: PropTypes.func.isRequired,
  hour: PropTypes.string.isRequired,
  minute: PropTypes.string.isRequired,
};

export default MinutePickerCell;
