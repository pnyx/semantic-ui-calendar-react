import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';

class MonthPickerCell extends Component {
  _onMonthClick = event => {
    const { onClick, month } = this.props;
    event.stopPropagation();
    onClick(event, { ...this.props, value: month });
  };
  render() {
    const { month, monthId, active, disabled } = this.props;
    return (
      <Table.Cell
        active={active}
        disabled={disabled}
        onClick={this._onMonthClick}
        className="suir-calendar date"
        textAlign="center"
        data-test={`month_${monthId + 1}`}
      >
        {month}
      </Table.Cell>
    );
  }
}

MonthPickerCell.propTypes = {
  /** (event, data) => {} */
  onClick: PropTypes.func.isRequired,
  month: PropTypes.string.isRequired,
  monthId: PropTypes.number,
};

export default MonthPickerCell;
