import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';
import { toLower } from 'lodash';

class YearPickerCell extends Component {
  _onYearClick = event => {
    const { onClick, year } = this.props;
    event.stopPropagation();
    onClick(event, { ...this.props, value: year });
  };
  render() {
    const { year, active, disabled } = this.props;
    return (
      <Table.Cell
        active={active}
        disabled={disabled}
        onClick={this._onYearClick}
        className="suir-calendar date"
        textAlign="center"
        data-test={`year_${toLower(year)}`}
      >
        {year}
      </Table.Cell>
    );
  }
}

YearPickerCell.propTypes = {
  onClick: PropTypes.func.isRequired,
  year: PropTypes.string.isRequired,
};
export default YearPickerCell;
