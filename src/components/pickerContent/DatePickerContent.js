import React from 'react';
import PropTypes from 'prop-types';

import MonthMode from '../pickerModes/MonthMode';
import DayMode from '../pickerModes/DayMode';
import YearMode from '../pickerModes/YearMode';

const DatePickerContent = ({
  mode,
  handleHeaderDateClick,
  onYearChange,
  showNextYear,
  showPrevYear,
  dateToShow,
  onMonthChange,
  showNextMonth,
  showPrevMonth,
  onDateClick,
  activeDate,
  yearsRange,
  showPrevYearsRange,
  showNextYearsRange,
  switchMode,
  closePopup,
  inputType,
}) => {
  if (mode === 'year') {
    return (
      <YearMode
        onHeaderDateClick={handleHeaderDateClick}
        yearsRange={yearsRange}
        showPrevYearsRange={showPrevYearsRange}
        showNextYearsRange={showNextYearsRange}
        onYearClick={onYearChange}
        mode={mode}
        dateToShow={dateToShow}
        closePopup={closePopup}
        inputType={inputType}
        switchMode={switchMode}
      />
    );
  }
  if (mode === 'month') {
    return (
      <MonthMode
        handleHeaderDateClick={handleHeaderDateClick}
        showPrevYear={showPrevYear}
        showNextYear={showNextYear}
        dateToShow={dateToShow}
        onMonthChange={onMonthChange}
        closePopup={closePopup}
        inputType={inputType}
        mode={mode}
        switchMode={switchMode}
      />
    );
  }
  return (
    <DayMode
      handleHeaderDateClick={handleHeaderDateClick}
      showNextMonth={showNextMonth}
      showPrevMonth={showPrevMonth}
      dateToShow={dateToShow}
      onDateClick={onDateClick}
      activeDate={activeDate}
      mode={mode}
      switchMode={switchMode}
      closePopup={closePopup}
      inputType={inputType}
    />
  );
};

DatePickerContent.propTypes = {
  mode: PropTypes.string,
  handleHeaderDateClick: PropTypes.func,
  onYearChange: PropTypes.func,
  showNextYear: PropTypes.func,
  showPrevYear: PropTypes.func,
  dateToShow: PropTypes.object,
  onMonthChange: PropTypes.func,
  showNextMonth: PropTypes.func,
  showPrevMonth: PropTypes.func,
  onDateClick: PropTypes.func,
  activeDate: PropTypes.object,
  yearsRange: PropTypes.object,
  switchMode: PropTypes.func,
  inputType: PropTypes.string,
  closePopup: PropTypes.func,
};
export default DatePickerContent;
