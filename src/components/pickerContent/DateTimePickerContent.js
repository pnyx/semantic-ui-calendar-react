import React from 'react';
import PropTypes from 'prop-types';
import { includes, cond, matches, constant, stubTrue } from 'lodash';

import { PickerHeader, TimePickerPopup } from '../';

import DatePickerContent from './DatePickerContent';

const DateTimePickerContent = ({
  activeDate,
  activeHour,
  activeMinute,
  mode,
  handleHeaderDateClick,
  handleHeaderTimeClick,
  onYearChange,
  showNextMonth,
  showPrevMonth,
  showNextYear,
  showPrevYear,
  showNextDay,
  showPrevDay,
  dateToShow,
  onMonthChange,
  onDateClick,
  onHourClick,
  onMinuteClick,
  yearsRange,
  showPrevYearsRange,
  showNextYearsRange,
  switchMode,
  closePopup,
}) => {
  const headerWidth = cond([
    [matches('minute'), constant('3')],
    [matches('hour'), constant('4')],
    [stubTrue, constant('7')],
  ])(mode);
  if (!includes(['hour', 'minute'], mode)) {
    return (
      <DatePickerContent
        mode={mode}
        handleHeaderDateClick={handleHeaderDateClick}
        onYearChange={onYearChange}
        showNextYear={showNextYear}
        showPrevYear={showPrevYear}
        showPrevYearsRange={showPrevYearsRange}
        showNextYearsRange={showNextYearsRange}
        dateToShow={dateToShow}
        onMonthChange={onMonthChange}
        showNextMonth={showNextMonth}
        showPrevMonth={showPrevMonth}
        onDateClick={onDateClick}
        activeDate={activeDate}
        yearsRange={yearsRange}
        switchMode={switchMode}
        closePopup={closePopup}
        inputType="dateTime"
      />
    );
  }
  return (
    <>
      <PickerHeader
        onDateClick={handleHeaderTimeClick}
        onNextBtnClick={showNextDay}
        onPrevBtnClick={showPrevDay}
        activeDate={activeDate}
        includeDay
        width={headerWidth}
        mode={mode}
      />
      <TimePickerPopup
        mode={mode}
        activeHour={activeHour}
        activeMinute={activeMinute}
        onHourClick={onHourClick}
        onMinuteClick={onMinuteClick}
        switchMode={switchMode}
        closePopup={closePopup}
        inputType="dateTime"
      />
    </>
  );
};

DateTimePickerContent.propTypes = {
  activeDate: PropTypes.object,
  activeHour: PropTypes.string,
  activeMinute: PropTypes.string,
  mode: PropTypes.string,
  dateToShow: PropTypes.object,
  handleHeaderDateClick: PropTypes.func,
  handleHeaderTimeClick: PropTypes.func,
  onYearChange: PropTypes.func,
  showNextMonth: PropTypes.func,
  showNextYear: PropTypes.func,
  showPrevMonth: PropTypes.func,
  showPrevYear: PropTypes.func,
  showNextDay: PropTypes.func,
  showPrevDay: PropTypes.func,
  onMonthChange: PropTypes.func,
  onDateClick: PropTypes.func,
  onHourClick: PropTypes.func,
  onMinuteClick: PropTypes.func,
  yearsRange: PropTypes.object,
  onPrevBtnClick: PropTypes.func,
  onNextBtnClick: PropTypes.func,
  switchMode: PropTypes.func,
  closePopup: PropTypes.func,
};
export default DateTimePickerContent;
