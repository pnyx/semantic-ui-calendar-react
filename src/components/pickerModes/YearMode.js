/* eslint-disable react/jsx-no-bind */
import React from 'react';
import PropTypes from 'prop-types';

import { PickerHeader, YearPickerPopup } from '../';

const YearMode = ({
  onHeaderDateClick,
  yearsRange,
  showPrevYearsRange,
  showNextYearsRange,
  onYearClick,
  dateToShow,
  switchMode,
  inputType,
  mode,
}) => {
  return (
    <>
      <PickerHeader
        width="3"
        onDateClick={onHeaderDateClick}
        activeYears={yearsRange}
        onPrevBtnClick={showPrevYearsRange}
        mode={mode}
        onNextBtnClick={showNextYearsRange}
      />
      <YearPickerPopup
        onYearClick={onYearClick}
        activeYear={dateToShow?.format('YYYY')}
        yearsStart={yearsRange.start}
        switchMode={switchMode}
        inputType={inputType}
      />
    </>
  );
};

YearMode.propTypes = {
  onHeaderDateClick: PropTypes.func,
  yearsRange: PropTypes.object,
  onPrevBtnClick: PropTypes.func,
  onNextBtnClick: PropTypes.func,
  onYearClick: PropTypes.func,
  switchMode: PropTypes.func,
  value: PropTypes.string,
  inputType: PropTypes.string,
};
export default YearMode;
