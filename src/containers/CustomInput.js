import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'semantic-ui-react';

import { dispatchDateChange } from '../lib/events';

const CustomInput = props => {
  const { value } = props;
  useEffect(() => {
    dispatchDateChange();
  }, [value]);
  return <Input {...props} />;
};

CustomInput.propTypes = {
  value: PropTypes.any,
};

export default CustomInput;
