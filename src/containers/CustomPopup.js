import React from 'react';
import { Popup } from 'semantic-ui-react';
import { omit } from 'lodash';

const CustomPopup = props => (
  <Popup
    {...omit(props, ['inputType'])}
    flowing
    id="suirCalendarPopup"
    on="click"
    className="suir-calendar popup"
    popperModifiers={{
      preventOverflow: {
        boundariesElement: 'viewport',
        escapeWithReference: true,
      },
    }}
  />
);

export default CustomPopup;
