import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import Toggle from 'react-toggled';
import { size, split, trim } from 'lodash';
import moment from 'moment';

import { DATES_RANGE_INPUT } from '../../lib/COMPONENT_TYPES';
import DatesRangePickerContent from '../../components/pickerContent/DatesRangePickerContent';
import { ControlledCustomPopup, CustomInput, withStateInput } from '../';

const NULL_DATE = `__-__-____`;
const NULL_INPUT=`${NULL_DATE} - ${NULL_DATE}`

const checkNullDate = (dateStr) => {
  const trimedText = trim(dateStr)
  return !trimedText || trimedText === NULL_DATE
}

const checkNullRange = (rangeStr) => {
  const trimedText = trim(rangeStr)
  return !trimedText || trimedText === '-'
}

const showRangeDate = ({ start, end, dateFormat }) => {
  const showStart = start ? start.format(dateFormat) : NULL_DATE;
  const showEnd = end ? end.format(dateFormat) : NULL_DATE;
  return `${showStart} - ${showEnd}`
}

const validateDatesRange = (date, dateFormat, onValidateError, onValidated) => {
  if (checkNullRange(date)) {
    if (onValidated) onValidated();
    return { start: null, end: null };
  }

  const splitData = split(date, ' - ');

  if (splitData.length == 2) {
    const startStr = trim(splitData[0])
    const endStr = trim(splitData[1])
    const start = moment(startStr, dateFormat, true);
    const end = moment(endStr, dateFormat, true);
    const isNullStart = checkNullDate(startStr)
    const isNullEnd = checkNullDate(endStr)
    if (start.isValid() && end.isValid()) {
      if (onValidated) onValidated();
      return { start, end };
    }
    if (isNullStart && isNullEnd) {
      if (onValidated) onValidated();
      return { start: null, end: null };
    }
    if (start.isValid() && isNullEnd) {
      if (onValidated) onValidated();
      return { start, end: null };
    }
    if (end.isValid() && isNullStart) {
      if (onValidated) onValidated();
      return { start: null, end };
    }
  }

  if (splitData.length == 1) {
    const text = trim(splitData[0]);
    const lengtText = size(text)
    if (checkNullRange(text)) {
      if (onValidated) onValidated();
      return { start: null, end: null };
    }
    if (text[0] === '-') {
      const restStr = trim(text.slice(1, lengtText));
      if (checkNullDate(restStr)) {
        if (onValidated) onValidated();
        return { start: null, end: null }
      }
      const end = moment(restStr, dateFormat, true);
      if (end.isValid()) {
        if (onValidated) onValidated();
        return { start: null, end }
      }
    }
    if (text[lengtText - 1] === '-') {
      const restStr = trim(text.slice(0, lengtText - 1));
      if (checkNullDate(restStr)) {
        if (onValidated) onValidated();
        return { start: null, end: null }
      }
      const start = moment(restStr, dateFormat, true);
      if (start.isValid()) {
        if (onValidated) onValidated();
        return { start, end: null }
      }
    }
    if (onValidateError) onValidateError();
  }

  if (onValidateError) onValidateError();
};

class CustomDatesRangeInput extends Component {
  static META = {
    type: DATES_RANGE_INPUT,
    name: 'CustomDatesRangeInput',
  };
  constructor(props) {
    super(props);
    const { datesRange } = props;
    if (datesRange) {
      const { start, end } = datesRange;
      const { dateFormat } = props;
      const datesRangeInputValue = showRangeDate({ start, end, dateFormat })
      this.state = {
        datesRangeInputValue,
      };
    }
  }
  componentWillReceiveProps = nextProps => {
    const { datesRange } = nextProps;
    if (datesRange) {
      const { start, end } = datesRange;
      const { dateFormat } = this.props;
      const datesRangeInputValue = start || end ? `${
        start ? start.format(dateFormat) : ''
      } - ${end ? end.format(dateFormat) : ''}` : '';
      this.setState({ datesRangeInputValue });
    }
  };
  getPicker() {
    const {
      handleHeaderDateClick,
      showNextMonth,
      showPrevMonth,
      dateToShow,
      datesRange,
      setDatesRange,
      switchMode,
      mode,
    } = this.props;
    return (
      <Table unstackable celled textAlign="center">
        <DatesRangePickerContent
          handleHeaderDateClick={handleHeaderDateClick}
          showNextMonth={showNextMonth}
          showPrevMonth={showPrevMonth}
          dateToShow={dateToShow}
          datesRange={datesRange}
          setDatesRange={setDatesRange}
          inputType="datesRange"
          mode={mode}
          switchMode={switchMode}
        />
      </Table>
    );
  }
  _onChange = e => {
    const value = e.target.value;
    const {
      dateFormat,
      setStartEndDatesRange,
      onValidateError,
      onValidated,
    } = this.props;
    const date = validateDatesRange(
      value,
      dateFormat,
      onValidateError,
      onValidated,
    );
    if (date) {
      setStartEndDatesRange(null, date);
    }
    this.setState({ datesRangeInputValue: value });
  };
  _onBlur = e => {
    const { datesRangeInputValue } = this.state;
    const inputValue = trim(datesRangeInputValue)
    if (checkNullRange(inputValue)) {
      const newValue = NULL_INPUT
      return this.setState({ datesRangeInputValue: newValue })
    }

    const splitData = split(inputValue, '-')
    if (size(splitData)===4) {
      if (!trim(splitData[3])) {
        const newValue = `${inputValue} ${NULL_DATE}`
        return this.setState({ datesRangeInputValue: newValue })
      }
      if (!trim(splitData[0])) {
        const newValue = `${NULL_DATE} ${inputValue}`
      return this.setState({ datesRangeInputValue: newValue })
      }
    }
  }
  _getInput = () => {
    const {
      className,
      iconPosition,
      mode,
      name,
      placeholder,
      icon,
    } = this.props;
    const { datesRangeInputValue } = this.state;
    

    return (
      <CustomInput
        className={className}
        iconPosition={iconPosition}
        mode={mode}
        name={name}
        placeholder={placeholder}
        ref={this.input}
        onChange={this._onChange}
        icon={icon}
        fluid
        value={datesRangeInputValue}
        onBlur={this._onBlur}
      />
    );
  };
  render() {
    const {
      popupPosition,
      inline,
      setDatesRange,
      setStartEndDatesRange,
      customTrigger,
    } = this.props;

    if (inline) {
      return this.getPicker();
    }
    return (
      <Toggle initial={false}>
        {({ on, setOff, setOn }) => (
          <ControlledCustomPopup
            position={popupPosition}
            trigger={customTrigger || this._getInput()}
            popupState={on}
            handleClose={setOff}
            handleOpen={setOn}
            setDatesRange={setDatesRange}
            setStartEndDatesRange={setStartEndDatesRange}
            inputType="datesRange">
            {this.getPicker()}
          </ControlledCustomPopup>
        )}
      </Toggle>
    );
  }
}

CustomDatesRangeInput.propTypes = {
  /** Called on change.
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props and proposed value.
   */
  /** Same as semantic-ui-react Input's ``icon`` prop. */
  icon: PropTypes.any,
  /** Date formatting string.
   * Anything that that can be passed to ``moment().format``.
   */
  onValidateError: PropTypes.func,
  onValidated: PropTypes.func,
  dateFormat: PropTypes.string,
  customTrigger: PropTypes.func,
  /** Character that used to divide dates in string. */
  popupPosition: PropTypes.oneOf([
    'top left',
    'top right',
    'bottom left',
    'bottom right',
    'right center',
    'left center',
    'top center',
    'bottom center',
  ]),
  inline: PropTypes.bool,
  handleHeaderDateClick: PropTypes.func,
  showNextMonth: PropTypes.func,
  showPrevMonth: PropTypes.func,
  dateToShow: PropTypes.object,
  datesRange: PropTypes.object,
  setDatesRange: PropTypes.func,
  setStartEndDatesRange: PropTypes.func,
  className: PropTypes.string,
  iconPosition: PropTypes.string,
  mode: PropTypes.string,
  name: PropTypes.string,
  switchMode: PropTypes.func,
  placeholder: PropTypes.string,
};

CustomDatesRangeInput.defaultProps = {
  icon: 'calendar',
  inline: false,
};

const WrappedDatesRangeInput = withStateInput(CustomDatesRangeInput);

export default WrappedDatesRangeInput;
