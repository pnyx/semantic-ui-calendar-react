import React, { useState, useEffect, useCallback } from 'react';
import { Table } from 'semantic-ui-react';
import { trim } from 'lodash';
import moment from 'moment';

import {
  CustomPopup as Popup,
  CustomInput as Input,
  withStateInput,
} from '../..';
// import YearPickerMixin from '../../yearPickerMixin';
import { DATE_TIME_INPUT } from '../../../lib/COMPONENT_TYPES';
import DateTimePickerContent from '../../../components/pickerContent/DateTimePickerContent';
import useYearPickerMixin from '../../../hooks/useYearPickerMixin.hook';

const validateDate = (date, dateFormat, onValidateError, onValidated) => {
  const mmDate = moment(trim(date), dateFormat, true);
  if (mmDate.isValid()) {
    if (onValidated) onValidated();
    return mmDate;
  }
  if (onValidateError) onValidateError();
};

const DateTimeInput = ({
  dateToShow,
  dateTimeFormat,
  nullMessage,
  dateTimeValue,
  activeHour,
  activeMinute,
  mode,
  handleHeaderDateClick,
  handleHeaderTimeClick,
  onYearChange,
  showNextMonth,
  showPrevMonth,
  showNextYear,
  showPrevYear,
  showNextDay,
  showPrevDay,
  onMonthChange,
  onDateClick,
  onHourClick,
  onMinuteClick,
  switchMode,
  className,
  iconPosition,
  name,
  placeholder,
  icon,
  inline,
  customTrigger,
  onDateTimeChange,
  onValidateError,
  onValidated,
}) => {
  const [isOpenPopup, setIsOpenPopup] = useState(false);
  const [strDateTimeValue, setStrDateTimeValue] = useState(
    dateTimeValue ? dateTimeValue.format(dateTimeFormat) : nullMessage,
  );
  const [yearsRange, nextYear, prevYear] = useYearPickerMixin(
    dateToShow.year() - 6,
  );
  const getPicker = () => (
    <Table unstackable celled textAlign="center">
      <DateTimePickerContent
        activeDate={dateTimeValue}
        dateToShow={dateTimeValue}
        activeHour={activeHour}
        activeMinute={activeMinute}
        mode={mode}
        handleHeaderDateClick={handleHeaderDateClick}
        handleHeaderTimeClick={handleHeaderTimeClick}
        onYearChange={onYearChange}
        showNextMonth={showNextMonth}
        showPrevMonth={showPrevMonth}
        showNextYear={showNextYear}
        showPrevYear={showPrevYear}
        showNextDay={showNextDay}
        showPrevDay={showPrevDay}
        onMonthChange={onMonthChange}
        onDateClick={onDateClick}
        onHourClick={onHourClick}
        onMinuteClick={onMinuteClick}
        switchMode={switchMode}
        closePopup={handleClosePopup}
        yearsRange={yearsRange}
        showPrevYearsRange={prevYear}
        showNextYearsRange={nextYear}
      />
    </Table>
  );
  useEffect(() => {
    if (dateTimeValue) {
      const strDate = dateTimeValue.format(dateTimeFormat);
      setStrDateTimeValue(strDate);
    }
  }, [dateTimeValue.format('LLL')]);
  const onChange = useCallback(e => {
    const value = e.target.value;
    const date = validateDate(
      value,
      dateTimeFormat,
      onValidateError,
      onValidated,
    );
    if (date) {
      onDateTimeChange(date);
    }
    setStrDateTimeValue(value);
  }, []);
  const handleOpenPopup = useCallback(() => {
    setIsOpenPopup(true);
  });
  const handleClosePopup = useCallback(() => {
    setIsOpenPopup(false);
  });
  const inputElement = (
    <Input
      className={className}
      iconPosition={iconPosition}
      mode={mode}
      name={name}
      placeholder={placeholder}
      icon={icon}
      value={strDateTimeValue}
      onChange={onChange}
      fluid
    />
  );

  if (inline) {
    return getPicker();
  }
  const trigger = customTrigger || inputElement;
  return (
    <>
      <Popup
        trigger={trigger}
        open={isOpenPopup}
        onClose={handleClosePopup}
        onOpen={handleOpenPopup}
        on="click"
        inputType="dateTime">
        <div className="content">{getPicker()}</div>
      </Popup>
    </>
  );
};
DateTimeInput.META = {
  type: DATE_TIME_INPUT,
  name: 'DateTimeInput',
};
DateTimeInput.defaultProps = {
  icon: 'calendar',
  inline: false,
  dateTimeFormat: 'DD/MM/YYYY',
};

const WrappedDateTimeInput = withStateInput(DateTimeInput);

export default WrappedDateTimeInput;
