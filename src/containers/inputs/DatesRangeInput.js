import React from 'react';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';

import { DATES_RANGE_INPUT } from '../../lib/COMPONENT_TYPES';
import DatesRangePickerContent from '../../components/pickerContent/DatesRangePickerContent';
import { CustomPopup, CustomInput, withStateInput } from '../';

class DatesRangeInput extends React.Component {
  static META = {
    type: DATES_RANGE_INPUT,
    name: 'DatesRangeInput',
  };

  getPicker() {
    const {
      handleHeaderDateClick,
      showNextMonth,
      showPrevMonth,
      dateToShow,
      datesRange,
      mode,
      setDatesRange,
    } = this.props;
    return (
      <Table unstackable celled textAlign="center">
        <DatesRangePickerContent
          handleHeaderDateClick={handleHeaderDateClick}
          showNextMonth={showNextMonth}
          showPrevMonth={showPrevMonth}
          dateToShow={dateToShow}
          datesRange={datesRange}
          mode={mode}
          setDatesRange={setDatesRange}
          inputType="datesRange"
        />
      </Table>
    );
  }

  render() {
    const { onChange, icon, popupPosition, inline } = this.props;

    const inputElement = <CustomInput onChange={onChange} icon={icon} />;
    if (inline) {
      return this.getPicker();
    }
    return (
      <CustomPopup position={popupPosition} trigger={inputElement}>
        {this.getPicker()}
      </CustomPopup>
    );
  }
}

DatesRangeInput.propTypes = {
  /** Called on change.
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props and proposed value.
   */
  onChange: PropTypes.func,
  /** Same as semantic-ui-react Input's ``icon`` prop. */
  icon: PropTypes.any,
  /** Date formatting string.
   * Anything that that can be passed to ``moment().format``.
   */
  dateFormat: PropTypes.string,
  /** Character that used to divide dates in string. */
  divider: PropTypes.string,
  popupPosition: PropTypes.oneOf([
    'top left',
    'top right',
    'bottom left',
    'bottom right',
    'right center',
    'left center',
    'top center',
    'bottom center',
  ]),
  inline: PropTypes.bool,
  handleHeaderDateClick: PropTypes.func,
  showNextMonth: PropTypes.func,
  showPrevMonth: PropTypes.func,
  dateToShow: PropTypes.object,
  datesRange: PropTypes.object,
  setDatesRange: PropTypes.func,
  onValidateError: PropTypes.func,
  onValidated: PropTypes.func,
};

DatesRangeInput.defaultProps = {
  icon: 'calendar',
  inline: false,
};

const WrappedDatesRangeInput = withStateInput(DatesRangeInput);

export default WrappedDatesRangeInput;
