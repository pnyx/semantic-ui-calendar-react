/* eslint-disable react/jsx-no-bind */
import React, { useState, useCallback } from 'react';
import { Table, Input } from 'semantic-ui-react';
import { trim } from 'lodash';
import PropTypes from 'prop-types';
import moment from 'moment';

import { CustomPopup } from '../';
import { tick } from '../../lib';
import { TimePickerPopup } from '../../components';

const TIME_FORMAT = 'hh:mm A';
const parseTime = (value, outFormat = TIME_FORMAT) => {
  if (value) return moment(value, TIME_FORMAT).format(outFormat);
  return '00';
};

const TimeInput = props => {
  const { value: mmValue, name, onValidateError, onValidated } = props;
  const [isOpenPopup, setIsOpenPopup] = useState(false);

  const value = mmValue.format(TIME_FORMAT);
  const [mode, setMode] = useState();
  const [inputValue, setInputValue] = useState(mmValue.format(TIME_FORMAT));

  const onPopupClose = () => {
    setMode('hour');
  };
  const handleChange = (e, { value: changedValue }) => {
    setInputValue(changedValue);
    const mmChangedValue = moment(trim(changedValue), TIME_FORMAT, true);
    if (mmChangedValue.isValid()) {
      onChange(mmChangedValue);
      if (onValidated) onValidated();
    } else {
      if (onValidateError) onValidateError();
    }
  };

  const onHourClick = (event, data) => {
    tick(() => {
      const newMMValue = mmValue.clone();
      newMMValue.hour(data.value);
      setMode('minute');
      onChange(newMMValue);
      setInputValue(newMMValue.format(TIME_FORMAT));
    });
  };

  const onMinuteClick = (event, data) => {
    const newMMValue = mmValue.clone();
    newMMValue.minute(data.value);
    onChange(newMMValue);
    setMode('minute');
    setInputValue(newMMValue.format(TIME_FORMAT));
    setIsOpenPopup(false);
  };

  const switchMode = mode => {
    setMode(mode);
  };

  const handleOpenPopup = useCallback(() => {
    setIsOpenPopup(true);
  });
  const handleClosePopup = useCallback(() => {
    setIsOpenPopup(false);
  });

  const getPicker = () => {
    const { closePopup } = props;
    const [activeHour, activeMinute] = [
      parseTime(value, 'HH'),
      parseTime(value, 'mm'),
    ];
    return (
      <Table unstackable celled textAlign="center">
        <TimePickerPopup
          mode={mode}
          activeHour={activeHour}
          activeMinute={activeMinute}
          onHourClick={onHourClick}
          onMinuteClick={onMinuteClick}
          inputType="time"
          switchMode={switchMode}
          closePopup={closePopup}
        />
      </Table>
    );
  };
  const { onChange, icon, popupPosition, inline } = props;

  const inputElement = (
    <Input value={inputValue} icon={icon} onChange={handleChange} name={name} />
  );
  if (inline) {
    return getPicker();
  }
  return (
    <CustomPopup
      // onClose={onPopupClose}
      inputType="time"
      position={popupPosition}
      open={isOpenPopup}
      onClose={handleClosePopup}
      onOpen={handleOpenPopup}
      trigger={inputElement}>
      {getPicker()}
    </CustomPopup>
  );
};

TimeInput.propTypes = {
  /** Called on change.
   * @param {SyntheticEvent} event React's original SyntheticEvent.
   * @param {object} data All props and proposed value.
   */
  onChange: PropTypes.func,
  /** Same as semantic-ui-react Input's ``icon`` prop. */
  icon: PropTypes.any,
  popupPosition: PropTypes.oneOf([
    'top left',
    'top right',
    'bottom left',
    'bottom right',
    'right center',
    'left center',
    'top center',
    'bottom center',
  ]),
  inline: PropTypes.bool,
  value: PropTypes.string,
};

TimeInput.defaultProps = {
  icon: 'time',
  inline: false,
};

export default TimeInput;
