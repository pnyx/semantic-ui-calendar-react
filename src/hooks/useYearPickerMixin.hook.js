import { useState, useCallback } from 'react';

const useYearPickerMixin = defaultYear => {
  const [yearValue, setYearValue] = useState(defaultYear);
  const nextYear = useCallback(() => {
    setYearValue(yearValue + 12);
  }, [yearValue, setYearValue]);

  const prevYear = useCallback(() => {
    setYearValue(yearValue - 12);
  }, [yearValue, setYearValue]);

  const yearsRange = {
    start: yearValue,
    end: yearValue + 11,
  };
  return [yearsRange, nextYear, prevYear];
};

export default useYearPickerMixin;
