import React, { useEffect, useState, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Input } from 'semantic-ui-react';
import moment from 'moment';

import { validateDate } from '../../utils/date.utils';

import DateInputPopup from './DateInputPopup.component';

const DateInput = ({
  value,
  dateFormat,
  onValidateError,
  onValidated,
  className,
  iconPosition,
  mode,
  name,
  placeholder,
  icon,
  popupPosition,
  onChange,
  customTrigger,
  nullMessage,
}) => {
  const [isOpenPopup, setIsOpenPopup] = useState(false);
  const [inputDateValue, setInputDateValue] = useState(value);
  useEffect(() => {
    if (value && value != nullMessage) {
      setInputDateValue(value.format(dateFormat));
    }
  }, [value]);
  const handleInputChange = useCallback(
    e => {
      const targetValue = e.target.value;
      setInputDateValue(targetValue);

      if (!targetValue || targetValue === '') {
        return onChange(null);
      }
      const checkedDate = validateDate({
        date: targetValue,
        dateFormat,
        onValidateError,
        onValidated,
      });
      if (checkedDate) {
        const finalDate = checkedDate.startOf('day');
        onChange(finalDate);
      }
    },
    [value, dateFormat, onChange, setInputDateValue],
  );

  const handlePopup = useCallback(
    state => () => {
      setIsOpenPopup(state);
    },
    [setIsOpenPopup],
  );

  const trigger =
    customTrigger ||
    useMemo(
      () => (
        <Input
          className={className}
          iconPosition={iconPosition}
          mode={mode}
          name={name}
          placeholder={value ? placeholder : nullMessage}
          icon={icon}
          value={inputDateValue}
          onChange={handleInputChange}
          fluid
        />
      ),
      [inputDateValue, value],
    );
  return (
    <DateInputPopup
      value={value || moment()}
      position={popupPosition}
      trigger={trigger}
      open={isOpenPopup}
      onClose={handlePopup(false)}
      onOpen={handlePopup(true)}
      onChange={onChange}
    />
  );
};

DateInput.propTypes = {
  onChange: PropTypes.func,
  icon: PropTypes.any,
  dateFormat: PropTypes.string,
  popupPosition: PropTypes.oneOf([
    'top left',
    'top right',
    'bottom left',
    'bottom right',
    'right center',
    'left center',
    'top center',
    'bottom center',
  ]),
  // inline: PropTypes.bool,
  onValidateError: PropTypes.func,
  onValidated: PropTypes.func,
  nullMessage: PropTypes.string,
};

DateInput.defaultProps = {
  icon: 'calendar',
  dateFormat: 'DD-MM-YYYY',
  // startMode: 'day',
  // inline: false,
  nullMessage: 'Null',
};

export default DateInput;
