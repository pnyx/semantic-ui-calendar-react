import React from 'react';

import { CustomPopup } from '../../containers';

import DateInputPopupContent from './DateInputPopupContent.component';

const DateInputPopup = ({
  trigger,
  value,
  onChange,
  open,
  popupPosition,
  onClose,
  onOpen,
}) => (
  <CustomPopup
    position={popupPosition}
    trigger={trigger}
    open={open}
    onClose={onClose}
    onOpen={onOpen}
    on="click"
    inputType="date">
    <DateInputPopupContent
      value={value}
      onChange={onChange}
      onClose={onClose}
    />
  </CustomPopup>
);

export default DateInputPopup;
