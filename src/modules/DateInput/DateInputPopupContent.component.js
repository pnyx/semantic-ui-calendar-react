import React from 'react';
import { Table } from 'semantic-ui-react';

import { DATE_INPUT } from '../../lib/COMPONENT_TYPES';
import { withStateInput } from '../../containers';
import DatePickerContent from '../../components/pickerContent/DatePickerContent';
import useYearPickerMixin from '../../hooks/useYearPickerMixin.hook';

const DateInputPopupContent = ({
  onClose,
  onOpen,
  setDatesRange,
  onYearChange,
  showNextYear,
  showPrevYear,
  dateToShow,
  onMonthChange,
  showNextMonth,
  showPrevMonth,
  onDateClick,
  activeDate,
  handleHeaderDateClick,
  mode,
  switchMode,
  onChange,
}) => {
  const [yearsRange, nextYear, prevYear] = useYearPickerMixin(
    dateToShow.year() - 6,
  );
  return (
    <Table unstackable celled textAlign="center">
      <DatePickerContent
        mode={mode}
        handleHeaderDateClick={handleHeaderDateClick}
        onYearChange={onYearChange}
        showNextYear={showNextYear}
        showPrevYear={showPrevYear}
        dateToShow={dateToShow}
        onMonthChange={onMonthChange}
        showNextMonth={showNextMonth}
        showPrevMonth={showPrevMonth}
        onDateClick={onDateClick}
        activeDate={activeDate}
        handleClosePopup={onClose}
        handleOpenPopup={onOpen}
        yearsRange={yearsRange}
        showPrevYearsRange={prevYear}
        showNextYearsRange={nextYear}
        onChange={onChange}
        setDatesRange={setDatesRange}
        closePopup={onClose}
        switchMode={switchMode}
        inputType="date"
      />
    </Table>
  );
};
DateInputPopupContent.META = {
  type: DATE_INPUT,
  name: 'DateInput',
};

export default withStateInput(DateInputPopupContent);
