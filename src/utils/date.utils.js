import moment from 'moment';
import { trim } from 'lodash';

export const validateDate = ({
  date,
  dateFormat,
  onValidateError,
  onValidated,
}) => {
  const mmDate = moment(trim(date), dateFormat, true);
  if (mmDate.isValid()) {
    if (onValidated) onValidated();
    return mmDate;
  }
  if (onValidateError) onValidateError();
};
